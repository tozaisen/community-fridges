steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            user_id serial not null primary key,
            first_name varchar(100) not null,
            pronouns varchar(150),
            email varchar(50) not null unique,
            hashed_password varchar(200) not null,
            type INT not null
            );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
    [
        # "UP" and "INSERT" SQL statement
        """
        CREATE TABLE items (
            id serial not null primary key,
            name varchar(200) not null
        );

        INSERT INTO items (
            name
            )
            VALUES
                (
                'water/drinks'
                ),
                (
                'sanitary items'
                ),
                (
                'prepared meals'
                ),
                (
                'pantry goods'
                ),
                (
                'cheese'
                ),
                (
                'frozen good'
                ),
                (
                'plasticware'
                ),
                (
                'fruit'
                ),
                (
                'meat'
                ),
                (
                'ice packs'
                ),
                (
                'vegetables'
                ),
                (
                'milk'
                ),
                (
                'grocery bags'
                ),
                (
                'bread'
                ),
                (
                'pastries'
                );
        """,
        # "Down" SQL statement
        """
        DELETE from items;
        DROP TABLE items;
        """,
    ],
    [
        # "Up" SQL statement
        """
        CREATE TABLE check_in_reasons (
            id serial not null primary key,
            name varchar(100) not null
        );
        INSERT INTO check_in_reasons (name)
        VALUES ('Adding Food'), ('Collecting Food'), ('Cleaning the Fridge/Pantry'), ('Checking Stock Levels');
        """,
        # "Down" SQL statement
        """
        DROP TABLE check_in_reasons;
        """,
    ],
    [
        """
        CREATE TABLE clean_statuses (
            id SERIAL PRIMARY KEY not NULL,
            clean_status_name VARCHAR(200) not NULL
        );

        INSERT INTO clean_statuses (clean_status_name)
        VALUES ('Needs Cleaning ASAP/Sucia'), ('A Little Dirty/Asi Asi'), ('Sparkly/Muy Limpio');

        """,
        # "Down" SQL statement
        """
        DROP TABLE clean_statuses;
        """,
    ],
    [
        """
        CREATE TABLE food_statuses (
            id serial not null primary key,
            food_status_name varchar(100) not null
            );

        INSERT INTO food_statuses (food_status_name)
        VALUES ('nearly empty'), ('room for more'), ('pretty full'), ('overflowing');

        """,
        # "Down" SQL statement
        """
        DROP TABLE food_statuses;
        """,
    ],
    [
        """
        CREATE TABLE neighborhoods (
            id serial not null primary key,
            neighborhood_name varchar(100) not null
            );
        INSERT INTO neighborhoods
            (neighborhood_name)
            VALUES
            ('Eagle Rock'),
            ('Los Feliz'),
            ('Silverlake'),
            ('El Sereno');
        """,
        """
        DROP TABLE neighborhoods;
        """,
    ],
    [
        """
        CREATE TABLE postal_codes (
            id serial not null primary key,
            postal_code varchar(10) not null unique,
            neighborhood_id int references neighborhoods(id)
            );
            INSERT INTO postal_codes
            (postal_code, neighborhood_id)
            VALUES
            ('90041', 1),
            ('90042', 1),
            ('90065', 1),
            ('90027', 2),
            ('90026', 3),
            ('90039', 3),
            ('90032', 4),
            ('90063', 4);
            ;
        """,
        # "Down" SQL statement
        """
        DROP TABLE postal_codes;
        """,
    ],
    [
        """
        CREATE TABLE fridges (
            id SERIAL PRIMARY KEY not NULL,
            active boolean not null,
            fridge_name VARCHAR(200) not NULL,
            fridge_picture TEXT,
            neighborhood INT REFERENCES neighborhoods (id),
            location VARCHAR(250) not NULL,
            hours_start TIME not NULL,
            hours_end TIME not NULL,
            date_created date not null DEFAULT CURRENT_DATE
        );
        INSERT INTO fridges
            ( active, fridge_name, fridge_picture, neighborhood, location, hours_start, hours_end, date_created)
            VALUES
            (FALSE, 'Eagle Rock Community Fridge', 'https://i.imgur.com/wEkTptm.jpg', 1, '5032 Maywood Ave, Los Angeles, CA 90041', '08:00:00', '16:00:00', '2023-07-19'),
            (TRUE, 'Los Feliz Community Fridge', 'https://i.imgur.com/HTlKH3w.jpg', 2, '1733 N New Hampshire Ave Los Angeles, CA 90027', '00:00:00', '23:59:00', '2023-07-19'),
            (TRUE, 'Silver Lake Community Fridge', 'https://i.imgur.com/95MSGW8.jpg', 3, '1515 Griffith Park Blvd Los Angeles, CA 90026', '07:30:00', '19:30:00', '2023-07-19'),
            (TRUE, 'El Sereno Community Fridge', 'https://i.imgur.com/pX0mpq6.jpg', 4, '5469 Huntington Dr N Los Angeles, CA 90032', '00:00:00', '23:59:00', '2023-07-19');

        """,
        """
        DROP TABLE fridges;
        """,
    ],
    [
        """
        CREATE TABLE fridge_checkins (
            id SERIAL PRIMARY KEY not NULL,
            fridge INT REFERENCES fridges (id) not NULL,
            date_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
            clean_status INT REFERENCES clean_statuses (id),
            food_status INT references food_statuses (id),
            picture TEXT,
            additional TEXT,
            visitor TEXT
           );
        """,
        """
        DROP TABLE fridge_checkins;
        """,
    ],
    [
        """
        CREATE TABLE pantries (
            id SERIAL PRIMARY KEY not NULL,
            active boolean not null,
            pantry_name varchar(200) not NULL,
            pantry_picture TEXT,
            neighborhood INT REFERENCES neighborhoods (id),
            location VARCHAR(250) not NULL,
            hours_start TIME not NULL,
            hours_end TIME not NULL,
            date_created date not null DEFAULT CURRENT_DATE

        );
        INSERT INTO pantries
            ( active, pantry_name, pantry_picture, neighborhood, location, hours_start, hours_end, date_created)
            VALUES
            (FALSE, 'Eagle Rock Community Pantry', 'https://cdn.pixabay.com/photo/2014/08/12/12/52/pantry-416596_1280.jpg', 1, '5032 Maywood Ave, Los Angeles, CA 90041', '08:00:00', '16:00:00', '2023-07-20'),
            (TRUE, 'Los Feliz Community Pantry', 'https://cdn.pixabay.com/photo/2021/02/20/15/38/pantry-6033796_1280.jpg', 2, '1733 N New Hampshire Ave Los Angeles, CA 90027', '00:00:00', '23:59:00', '2023-07-20'),
            (TRUE, 'Silver Lake Community Pantry', 'https://i.insider.com/5e6e4d27235c1814256da892?width=700', 3, '1515 Griffith Park Blvd Los Angeles, CA 90026', '07:30:00', '19:30:00', '2023-07-20'),
            (TRUE, 'El Sereno Community Pantry', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTBF-03_ns5BqqFj8UXIAp9b373xcPS55gV2w&usqp=CAU', 4, '5469 Huntington Dr N Los Angeles, CA 90032', '00:00:00', '23:59:00', '2023-07-20');

        """,
        """
        DROP TABLE pantries;
        """,
    ],
    [
        """
        CREATE TABLE pantry_requests(
            id serial not null primary key,
            pantry int REFERENCES pantries (id),
            date_created date not null DEFAULT CURRENT_DATE,
            additional varchar(500),
            visitor varchar(100)
        );
        """,
        """
        DROP TABLE pantry_requests;
        """,
    ],
    [
        """
        CREATE TABLE pantry_request_items (
        pantry_request_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (pantry_request_id) REFERENCES pantry_requests (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (pantry_request_id, item_id)
        );
        """,
        """
        DROP TABLE pantry_request_items;
        """,
    ],
    [
        """
        CREATE TABLE fridge_checkin_reasons (
        fridge_checkin_id INT NOT NULL,
        reason_id INT NOT NULL,
        FOREIGN KEY (fridge_checkin_id) REFERENCES fridge_checkins (id),
        FOREIGN KEY (reason_id) REFERENCES check_in_reasons (id),
        PRIMARY KEY (fridge_checkin_id, reason_id)
        );
        """,
        """
        DROP TABLE fridge_checkin_reasons;
        """,
    ],
    [
        """
        CREATE TABLE fridge_checkin_items (
        fridge_checkin_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (fridge_checkin_id) REFERENCES fridge_checkins (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (fridge_checkin_id, item_id)
        );
        """,
        """
        DROP TABLE fridge_checkin_items;
        """,
    ],
    [
        """
        CREATE TABLE fridge_requests(
            id serial not null primary key,
            fridge int REFERENCES fridges (id),
            date_created date not null DEFAULT CURRENT_DATE,
            additional varchar(500),
            visitor varchar(100)
        );
        """,
        """
        DROP TABLE fridge_requests;
        """,
    ],
    [
        """
        CREATE TABLE fridge_request_items (
        fridge_request_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (fridge_request_id) REFERENCES fridge_requests (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (fridge_request_id, item_id)
        );
        """,
        """
        DROP TABLE fridge_request_items;
        """,
    ],
    [
        """
        CREATE TABLE pantry_check_ins (
            id SERIAL PRIMARY KEY NOT NULL,
            pantry_id INT REFERENCES pantries (id),
            date_time TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
            food_status INT REFERENCES food_statuses (id),
            clean_status INT REFERENCES clean_statuses (id),
            photo varchar(1000),
            Anything_else_details varchar(100),
            name_or_initials varchar(50)
        );
        """,
        """
        DROP TABLE pantry_check_ins;
        """,
    ],
    [
        """
        CREATE TABLE pantry_checkin_reasons (
        pantry_checkin_id INT NOT NULL,
        reason_id INT NOT NULL,
        FOREIGN KEY (pantry_checkin_id) REFERENCES pantry_check_ins (id),
        FOREIGN KEY (reason_id) REFERENCES check_in_reasons (id),
        PRIMARY KEY (pantry_checkin_id, reason_id)
        );
        """,
        """
        DROP TABLE pantry_checkin_reasons;
        """,
    ],
    [
        """
        CREATE TABLE pantry_checkin_items (
        pantry_checkin_id INT NOT NULL,
        item_id INT NOT NULL,
        FOREIGN KEY (pantry_checkin_id) REFERENCES pantry_check_ins (id),
        FOREIGN KEY (item_id) REFERENCES items (id),
        PRIMARY KEY (pantry_checkin_id, item_id)
        );
        """,
        """
        DROP TABLE pantry_checkin_items;
        """,
    ],
]
