from fastapi import APIRouter, Depends, HTTPException, status
from typing import Union
from queries.healthcheck import HealthStatusRepo, HealthCheck, Error


router = APIRouter()


@router.get(
    "/health", tags=["Health Status"], response_model=Union[HealthCheck, Error]
)
def read_health_status(
    repo: HealthStatusRepo = Depends(),
):
    try:
        return repo.get()
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e)
        )
