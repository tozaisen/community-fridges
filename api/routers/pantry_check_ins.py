from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.pantry_check_ins import (
    Error,
    PantryCheckIn,
    PantryCheckInsRepository,
    PantryCheckOut,
)
from authenticator import authenticator

router = APIRouter()


@router.post(
    "/pantry_check_ins/",
    tags=["Pantry Check-Ins"],
    response_model=Union[PantryCheckOut, Error],
)
def create_a_pantry_check_in(
    pantry_check: PantryCheckIn,
    response: Response,
    repo: PantryCheckInsRepository = Depends(),
):
    result = repo.create(pantry_check)
    if result is None:
        response.status_code = 404
        return Error(message="Unable to create pantry check-in")
    return result


@router.get(
    "/pantry-check-ins/",
    tags=["Pantry Check-Ins"],
    response_model=Union[List[PantryCheckOut], Error],
)
def get_all_pantry_check_ins(
    response: Response,
    repo: PantryCheckInsRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to view check-ins.")
    return repo.get_all()


@router.get(
    "/api/pantrycheckins/{id}",
    tags=["Pantry Check-Ins"],
    response_model=Union[List[PantryCheckOut], Error],
)
def get_checkins_for_a_pantry(
    id: int,
    response: Response,
    repo: PantryCheckInsRepository = Depends(),
) -> PantryCheckOut | Error:
    result = repo.get_all_checkins_for_pantry(id)
    if result is None:
        response.status_code = 404
        result = Error(message="Unable to get checkins")
    return result
