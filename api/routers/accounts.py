from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from pydantic import BaseModel
from typing import List, Union
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from queries.accounts import (
    Error,
    AccountIn,
    UserUpdate,
    AccountOut,
    AccountRepo,
    DuplicateAccountError,
)


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.post(
    "/api/users/",
    tags=["Users"],
    response_model=AccountToken | HttpError,
)
async def create_a_user(
    info: AccountIn,
    request: Request,
    response: Response,
    repo: AccountRepo = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = AccountOut(**repo.create(info, hashed_password).dict())
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.email, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    print(AccountToken(account=account, **token.dict()))
    return AccountToken(account=account, **token.dict())


@router.put(
    "/users/{user_id}/",
    tags=["Users"],
    response_model=Union[AccountOut, Error],
)
async def update_one_user(
    user_id: int,
    user: UserUpdate,
    response: Response,
    repo: AccountRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[Error, AccountOut]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a user.")
    return repo.update(user_id, user)


@router.get(
    "/token", tags=["Authentication"], response_model=AccountToken | None
)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
):
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.get(
    "/users/", tags=["Users"], response_model=Union[List[AccountOut], Error]
)
async def get_all_users(
    response: Response,
    repo: AccountRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[AccountOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to get a specific user.")
    result = repo.get_all()
    if result is None:
        response.status_code = 404
        result = Error(message="No users exist.")
    return result


@router.get("/users/{user_id}/", tags=["Users"])
async def get_one_user(
    user_id: int,
    response: Response,
    users: AccountRepo = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[AccountOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to get a specific user.")
    result = users.get_one(user_id)
    if result is None:
        response.status_code = 404
        result = Error(
            message=f"A user with the user_id {user_id} was not found."
        )
    return result


@router.delete("/users/{user_id}", tags=["Users"], response_model=bool)
async def delete_user(
    user_id: int,
    repo: AccountRepo = Depends(),
) -> bool:
    return repo.delete(user_id)
