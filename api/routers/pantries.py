from fastapi import APIRouter, Depends, Response
from typing import List, Union
from queries.pantries import (
    Error,
    PantryIn,
    PantryOut,
    PantryRepository,
)


from authenticator import authenticator


router = APIRouter()


@router.post(
    "/pantries", tags=["Pantries"], response_model=Union[PantryOut, Error]
)
async def create_pantry(
    pantry: PantryIn,
    response: Response,
    repo: PantryRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to create pantries.")

    result = repo.create(pantry)
    if result is None:
        response.status_code = 400
        result = Error(message="Unable to create new pantry.")
    else:
        return result


@router.get(
    "/pantries",
    tags=["Pantries"],
    response_model=Union[List[PantryOut], Error],
)
async def get_all(
    response: Response,
    repo: PantryRepository = Depends(),
):
    result = repo.get_all()
    if result is None:
        response.status_code = 400
        result = Error(message="Unable to get all pantries")
    return result


@router.put(
    "/pantries/{pantry_id}",
    tags=["Pantries"],
    response_model=Union[PantryOut, Error],
)
def update_pantry(
    pantry_id: int,
    pantry: PantryIn,
    response: Response,
    repo: PantryRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> Union[PantryOut, Error]:
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to update a pantry.")
    result = repo.update_pantry(pantry_id, pantry)
    if "message" in result:
        response.status_code = 400
    else:
        response.status_code = 200
    return result


@router.delete(
    "/pantries/{pantry_id}/", tags=["Pantries"], response_model=bool
)
async def delete_a_pantry(
    listing_id: int,
    response: Response,
    repo: PantryRepository = Depends(),
    account: dict = Depends(authenticator.try_get_current_account_data),
):
    if account is None:
        response.status_code = 401
        return Error(message="Sign in to delete a pantry.")

    result = repo.delete_a_pantry(listing_id)
    if result is False:
        response.status_code = (400,)
        result = Error(message="unable to process delete request")

    return result


@router.get(
    "/pantries/{pantry_id}",
    tags=["Pantries"],
)
async def get_a_pantry(
    pantry_id: int,
    pantries: PantryRepository = Depends(),
) -> PantryOut:
    pantry = pantries.get_a_pantry(pantry_id)
    return pantry
