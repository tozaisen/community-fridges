from fastapi.testclient import TestClient
from main import app
from queries.items import ItemRepository

client = TestClient(app)


class EmptyItemQueries:
    def get_all(self):
        return []


def test_get_all_items():
    app.dependency_overrides[ItemRepository] = EmptyItemQueries
    response = client.get("/items/")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []
