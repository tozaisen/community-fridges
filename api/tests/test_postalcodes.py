from fastapi.testclient import TestClient
from main import app
from queries.postal_codes import PostalCodeIn, PostalCodeOut, PostalCodeRepo
from authenticator import authenticator

client = TestClient(app)


class MockPostalCodeRepo(PostalCodeRepo):
    def get_all(self):
        return [
            PostalCodeOut(id=1, postal_code="90039", neighborhood_id=1),
            PostalCodeOut(id=2, postal_code="90041", neighborhood_id=2),
        ]

    def create(self, code: PostalCodeIn) -> PostalCodeOut:
        return PostalCodeOut(
            id=5,
            postal_code=code.postal_code,
            neighborhood_id=code.neighborhood_id,
        )

    def update(self, postal_code_id: int, code: PostalCodeIn) -> PostalCodeOut:
        return PostalCodeOut(
            id=postal_code_id,
            postal_code=code.postal_code,
            neighborhood_id=code.neighborhood_id,
        )


def mock_authentication_successful():
    return {
        "user_id": 5,
        "first_name": "Sam",
        "pronouns": "He/him/his",
        "email": "sam@sam.com",
        "type": 1,
    }


def mock_authentication_failed():
    return None


def test_get_postal_codes():
    app.dependency_overrides[PostalCodeRepo] = MockPostalCodeRepo

    response = client.get("/postal_codes")
    assert response.status_code == 200
    assert response.json() == [
        {"id": 1, "postal_code": "90039", "neighborhood_id": 1},
        {"id": 2, "postal_code": "90041", "neighborhood_id": 2},
    ]

    app.dependency_overrides = {}


def test_create_postal_code():
    app.dependency_overrides[PostalCodeRepo] = MockPostalCodeRepo

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_authentication_successful

    response = client.post(
        "/postal_codes", json={"postal_code": "90042", "neighborhood_id": 3}
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 5,
        "postal_code": "90042",
        "neighborhood_id": 3,
    }

    app.dependency_overrides = {}


def test_update_postal_code():
    app.dependency_overrides[PostalCodeRepo] = MockPostalCodeRepo

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_authentication_successful

    response = client.put(
        "/postal_codes/5", json={"postal_code": "90043", "neighborhood_id": 4}
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 5,
        "postal_code": "90043",
        "neighborhood_id": 4,
    }

    app.dependency_overrides = {}


def test_unauthorized_create_postal_code():
    app.dependency_overrides[PostalCodeRepo] = MockPostalCodeRepo

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_authentication_failed

    response = client.post(
        "/postal_codes", json={"postal_code": "90042", "neighborhood_id": 3}
    )
    assert response.status_code == 401
    assert response.json() == {"message": "Sign in to create a postal code."}

    app.dependency_overrides = {}


def test_unauthorized_update_postal_code():
    app.dependency_overrides[PostalCodeRepo] = MockPostalCodeRepo

    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_authentication_failed

    response = client.put(
        "/postal_codes/5", json={"postal_code": "90043", "neighborhood_id": 4}
    )
    assert response.status_code == 401
    assert response.json() == {"message": "Sign in to update a postal code."}

    app.dependency_overrides = {}
