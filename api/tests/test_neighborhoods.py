from fastapi.testclient import TestClient
from main import app
from queries.neighborhoods import NeighborhoodRepo
from authenticator import authenticator
from pydantic import BaseModel

client = TestClient(app)


class EmptyNeighborhoodQueries:
    def get_all(self):
        return []


class CreateNeighborhoodQueries:
    def create(self, neighborhood):
        result = {"id": 34}
        result.update(neighborhood)
        return result


class UpdateNeighborhoodQueries:
    def update(self, neighborhood_id, neighborhood):
        result = {"id": neighborhood_id}
        result.update(neighborhood)
        return result


class AccountOut(BaseModel):
    user_id: int
    first_name: str
    pronouns: str
    email: str
    type: int


def fake_get_current_account_data():
    return AccountOut(
        user_id="10",
        first_name="CAL",
        pronouns="they/them",
        email="cal@cal.com",
        type=0,
    )


def test_get_neighborhoods():
    app.dependency_overrides[NeighborhoodRepo] = EmptyNeighborhoodQueries
    response = client.get("/neighborhoods")

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == []


def test_create_neighborhood():
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[NeighborhoodRepo] = CreateNeighborhoodQueries

    json = {"neighborhood_name": "Torrance"}

    expected = {"id": 34, "neighborhood_name": "Torrance"}

    response = client.post("/neighborhoods", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected


def test_update_neighborhood():
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = fake_get_current_account_data
    app.dependency_overrides[NeighborhoodRepo] = UpdateNeighborhoodQueries

    json = {"neighborhood_name": "El Paso"}

    expected = {"id": 13, "neighborhood_name": "El Paso"}

    response = client.put("/neighborhoods/13", json=json)

    app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == expected
