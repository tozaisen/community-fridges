from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    accounts,
    fridges,
    neighborhoods,
    items,
    food_status,
    checkinreasons,
    pantries,
    pantry_requests,
    postal_codes,
    fridge_checkins,
    clean_status,
    fridge_requests,
    pantry_check_ins,
    healthcheck,
)
from authenticator import authenticator

import os

tags_metadata = [
    {
        "name": "Authentication",
        "description": "Create users and get bearer token.",
    },
    {
        "name": "Users",
        "description": "Manage Users.",
    },
    {
        "name": "Neighborhoods",
        "description": "Manage neighborhoods.",
    },
    {
        "name": "Fridges",
        "description": "Manage fridges",
    },
    {
        "name": "Pantries",
        "description": "Manage pantries.",
    },
    {
        "name": "Pantry Requests",
        "description": "Manage Pantry Requests.",
    },
    {
        "name": "Pantry Check-Ins",
        "description": "Manage Pantry Requests.",
    },
    {
        "name": "Fridge Check-Ins",
        "description": "Manage Fridge Check-Ins",
    },
    {
        "name": "Fridge Requests",
        "description": "Manage Fridge Requests",
    },
    {
        "name": "Items",
        "description": "Manage items for check ins and item requests.",
    },
    {
        "name": "Food Statuses",
        "description": "Manage food statuses.",
    },
    {
        "name": "Clean Statuses",
        "description": "Manage Check-In Reasons.",
    },
    {
        "name": "Check-In Reasons",
        "description": "Manage Check-In Reasons.",
    },
    {
        "name": "Postal Codes",
        "description": "Manage postal codes.",
    },
    {
        "name": "Health Check",
        "description": "Return HTTP Status Code 200",
    },
]

app = FastAPI(
    title="Community Fridges & Pantries",
    description="These are the back-end endpoints used by the front-end of Community Fridges.",
    openapi_tags=tags_metadata,
)


app.include_router(accounts.router)
app.include_router(items.router)
app.include_router(authenticator.router)
app.include_router(pantries.router)
app.include_router(neighborhoods.router)
app.include_router(fridges.router)
app.include_router(clean_status.router)
app.include_router(food_status.router)
app.include_router(checkinreasons.router)
app.include_router(pantry_requests.router)
app.include_router(postal_codes.router)
app.include_router(fridge_checkins.router)
app.include_router(fridge_requests.router)
app.include_router(pantry_check_ins.router)
app.include_router(healthcheck.router)

origins = [os.environ.get("CORS_HOST", None), "http://localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}
