from fastapi import status
from pydantic import BaseModel


class HealthCheck(BaseModel):
    status: int


class Error(BaseModel):
    message: str


class HealthStatusRepo:
    def get(self) -> HealthCheck:
        return HealthCheck(status=status.HTTP_200_OK)
