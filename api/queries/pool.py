import os
from psycopg_pool import ConnectionPool

ca_file_content = None
if os.environ.get("POSTGRES_SSL"):
    with open("../us-east-1-bundle.pem", "r") as ca_file:
        ca_file_content = ca_file.read()

pool = ConnectionPool(
    conninfo=os.environ["DATABASE_URL"],
    # kwargs={
    #     "sslmode": "require" if os.environ.get("POSTGRES_SSL") else "disable",
    #     "sslrootcert": ca_file_content,
    # },
)

# DATABASE_URL = "postgresql+psycopg2://username:password@your-database-endpoint:port/database-name?sslmode=require&sslrootcert=/path/to/ca-certificate.pem"
