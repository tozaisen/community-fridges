from pydantic import BaseModel
from queries.pool import pool
from typing import Optional, Union, List


class Error(BaseModel):
    message: str
    status: Optional[int]


class DuplicateAccountError(ValueError):
    pass


class UserUpdate(BaseModel):
    first_name: str
    pronouns: str
    email: str
    type: int


class AccountIn(BaseModel):
    first_name: str
    pronouns: str
    email: str
    password: str
    type: int


class AccountOut(BaseModel):
    user_id: int
    first_name: str
    pronouns: str
    email: str
    type: int


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepo:
    def record_to_user_out(self, record) -> AccountOutWithPassword:
        account_dict = {
            "user_id": record[0],
            "first_name": record[1],
            "pronouns": record[2],
            "email": record[3],
            "hashed_password": record[4],
            "type": record[5],
        }
        return account_dict

    def user_in_to_out(self, user_id: int, account: UserUpdate):
        old_data = account.dict()
        return AccountOut(id=user_id, **old_data)

    def create(
        self, user: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (
                            first_name
                            ,pronouns
                            ,email
                            ,hashed_password
                            ,type)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING
                        user_id,
                        first_name,
                        pronouns,
                        email,
                        hashed_password,
                        type;
                        """,
                        [
                            user.first_name,
                            user.pronouns,
                            user.email,
                            hashed_password,
                            user.type,
                        ],
                    )
                    user_id = result.fetchone()[0]
                    return AccountOutWithPassword(
                        user_id=user_id,
                        first_name=user.first_name,
                        pronouns=user.pronouns,
                        email=user.email,
                        hashed_password=hashed_password,
                        type=user.type,
                    )

        except Exception:
            return {"message": "Could not create a user"}

    def update(
        self, user_id: int, user: UserUpdate
    ) -> Union[AccountOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE users
                        SET first_name = %s
                        , pronouns = %s
                        , email = %s
                        , type = %s
                        WHERE user_id = %s
                        RETURNING
                        user_id,
                        first_name,
                        pronouns,
                        email,
                        type;
                        """,
                        [
                            user.first_name,
                            user.pronouns,
                            user.email,
                            user.type,
                            user_id,
                        ],
                    )
                    user_id = result.fetchone()[0]
                    return AccountOut(
                        user_id=user_id,
                        first_name=user.first_name,
                        pronouns=user.pronouns,
                        email=user.email,
                        type=user.type,
                    )
        except Exception as e:
            if "duplicate key value violates unique constraint" in str(e):
                return {
                    "message": f"A user with the email {user.email} already exists."
                }
            else:
                return {"message": "Could not update that user."}

    def get_all(self) -> Union[Error, List[AccountOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT user_id, first_name, pronouns, email, hashed_password, type
                        FROM users
                        ORDER BY user_id;
                        """
                    )
                    return [
                        self.record_to_user_out(record) for record in result
                    ]
        except Exception:
            return {"message": "Could not get all users."}

    def get_one(self, user_id: int) -> Optional[AccountOutWithPassword]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                        user_id,
                        first_name,
                        pronouns,
                        email,
                        hashed_password,
                        type
                        FROM users
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_user_out(record)
        except Exception:
            return {"message": "Could not get that user"}

    def delete(self, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            DELETE FROM users
                            WHERE user_id = %s
                            """,
                        [user_id],
                    )
                    return True
        except Exception:
            return False

    def get(self, email: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            SELECT
                            user_id,
                            first_name,
                            pronouns,
                            email,
                            hashed_password,
                            type
                            FROM users
                            WHERE email = %s
                            """,
                        [email],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_user_out(record)
        except Exception:
            return {"message": "Could not get account"}
