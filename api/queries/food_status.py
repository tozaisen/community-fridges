from pydantic import BaseModel
from typing import Optional, List, Union
from queries.pool import pool


class FoodStatusIn(BaseModel):
    food_status_name: str


class FoodStatusOut(BaseModel):
    id: int
    food_status_name: str


class Error(BaseModel):
    message: str


class FoodStatusRepo:
    def food_status_in_to_out(self, id: int, food_status: FoodStatusIn):
        old_data = food_status.dict()
        return FoodStatusOut(id=id, **old_data)

    def record_to_food_status_out(self, record):
        return FoodStatusOut(
            id=record[0],
            food_status_name=record[1],
        )

    def get_all(self) -> Union[List[FoodStatusOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, food_status_name
                        FROM food_statuses
                        """
                    )
                    return [
                        self.record_to_food_status_out(record)
                        for record in result
                    ]
        except Exception as e:
            print(e)
            return {"message": "Could not retrieve all food statuses."}

    def create(self, food_status: FoodStatusIn) -> FoodStatusOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO food_statuses (food_status_name)
                        VALUES (%s)
                        RETURNING id;
                        """,
                        [food_status.food_status_name],
                    )
                    id = result.fetchone()[0]
                    return self.food_status_in_to_out(id, food_status)
        except Exception as e:
            print(e)
            return {"message": "Could not create food status."}

    def update(
        self, food_status_id: int, food_status: FoodStatusIn
    ) -> Union[FoodStatusOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE food_statuses
                        SET food_status_name = %s
                        WHERE id = %s
                        """,
                        (food_status.food_status_name, food_status_id),
                    )
                    return self.food_status_in_to_out(
                        food_status_id, food_status
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not update food status."}

    def delete(self, food_status_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM food_statuses
                        WHERE id = %s
                        """,
                        [food_status_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_one(self, food_status_id: int) -> Optional[FoodStatusOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , food_status_name
                        FROM food_statuses
                        WHERE id = %s
                        """,
                        [food_status_id],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_food_status_out(record)
        except Exception as e:
            print(e)
            return {"message": "Could not find food status."}
