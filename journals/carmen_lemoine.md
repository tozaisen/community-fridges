## THIS BE MY JOURNAL

## July 27, 2023

I was not feeling well today with a migraine. Luckily, group was sympathetic and finished the README while I went and rested. Felt better by the afternoon and was able to fix the delete button on the Users List and helped Preeti with the styling on Pantry Checkins List. After 5pm, Maria and I stayed to double check for errors. The Fridge Requests List seems not to load immediately after logging in, but it does load if you refresh the page. We think it has to do with the 'if (!token)' logic at the beginning of the return statement. It was Jahaziel's page so we didn't want to make changes.

## July 25, 2023

Yesterday, we did a bunch of merge requests before starting the deployment process. It took the whole day almost. I did have time to write some unit tests for neighborhood endpoints: get_all, create, and update. I'm not sure I did the update test correctly but it did pass. Today was the big day for deployment. We managed to get it completed today. However, it was a battle hard-fought and won. I think the most challenging part was making sure our paths were formatted correctly. We did run into errors that other groups had encountered and put in the HMU and so we were able to fix them quickly and move on. One thing that I don't like about deployment is how long the process takes when we make changes and merge them. The pipeline now runs for a few minutes before we see changes. I also styled a very slick homepage with a couple of lottie files. I'm especially proud that I was able to get them to show on the "cards": one for fridges and one for pantries. Pretty tired now and will tackle more styling and other small details tomorrow.

## July 20, 2023

okayyyyyyyyy... In the past week, we completed our backend endpoints. We started with frontend auth, for which I got to drive for the Signup component. Very interesting and I learned a lot about reading and _understanding_ the docs. Since then, I created a page for the Users List and just finished my form for pantry check-ins. However, I cannot test it yet because we had to do quite a bit of refactoring the migration table for pantries and so the endpoints now also need to be refactored. All that is left of my components is list of pantry item requests. Since it's a list, I'm hopeful that it won't take too long.

## July 13, 2023

We are just a smidge away from completing all backend endpoints. The last one is fridge check-ins, which we are going to complete together since there are many-to-many relationships. Then, we are going to make a plan for the frontend, choose a CSS library to work with and get started. Feels good to have a big chunk done.

## July 10, 2023

Today, we spent quite a bit of time pushing and pulling changes from our work branches. Then, pulling from remote main to local main. It's still a little confusing but I am feeling more comfortable with it. Over the break, I completed my Neighborhood endpoints and submitted a merge request. The code was mostly ok; just a few edits needed. I started and completed the endpoints for Food Statuses. When trying out the endpoints in the docs, however, the request body for 'create food status' says there should be a "repo" and I can't find where that's coming from. It shouldn't be there. I decided to leave it alone for now and look at it tomorrow. Onwards!

## June 29, 2023

We finished setting up the database and started working on setting up user accounts with JWTdown. We ran into some problems when we tried creating a user: it seems like the AccountForm is not formatted correctly. I looked at the JWTdown docs which led me to the OAuth2 docs and the OAuth2PasswordRequestForm. Hopefully, we can figure it out tomorrow.

## June 28, 2023

We finished git issues and discussed how to update our journals with git.
We decided to use a branch just for journals: notebook-branch.

We ran into some problems setting up the separate branch but ChatGPT helped Maria and us get through it. We did not get as far as we hoped but any experience grappling with git is good, in my opinion.

Tomorrow:
Start with database set up first, then Auth.
