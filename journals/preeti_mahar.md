July 22-23, 2023: here we are studying for practise test.Writing Readme.
July 21, 2023:
Completed PantryCheckIn List and sent the MR to Martinez.
Also wrote the test in the same branch.Did: pip install pytest ,in the docker for fast-api container
Ran: python -m pytest ,for running unit tests
Also completed PantryRequestForm and sent its MR.
July 20, 2023:
Today i am going to work on the pantryCheckIn list.Also, i worked on making drop-down menu for nav but couldn't figure that one out also.
So, kept it back to its original shape.

July 19, 2023:
Finally i was able to complete my add PantryForm, It took me rather long as i was going through some stupid errors with React! I had the aha moment when i was getting unprocessible entity at the front-end because the boolean value was set to True while it had to be true as it was javascript! So, felt dumb to have not figured that one out earlier.
Also, i figured out that, had to:
Install Flowbite  by running the following command in the Docker terminal:
'''npm install flowbite'''
to check Martinez's code and later to accept his merge after i verified his code!

July 18, 2023: Sam changed some of my backend code which was already running according to his frontend requirements.
I did work on designing Homepage and App.js.

July 16, 2023:
Also, most of the team decided which Ui/Ux we are supposed to use for which forms.
We are using Tailwindcss for it!

July 15, 2023:Working on dividing issues and the screens to design!

July 14, 2023:
issues creation on GitLab.

July 13, 2023:
discussion on front-end using react

July 12, 2023:
getting MR approved
for clean statuses

July 11, 2023:
getting MR approved for fridges.

July 10, 2023:
creating MR for fridges
adding tags to the routers/fridges.py file

July 4, 2023:

Reading abt redux

July 3, 2023:

Today i worked on :
Finishing the end-points for fridges

July 2, 2023:

Creating the endpoints for create, get and delete!
also get_all and update!

July 1, 2023:

Setting up Beekeeper as the default database.
Our username was admin and password was secret.
Setting up pg-admin also but realising that beekeeper is easier to work with!
Creating the table for fridges!

June 30, 2023:

Discussion about keeping the signing key a secret by adding it to a environ.env file.
Later adding that .env file to .gitignore folder to keep it secret!

June 29, 2023:

Setting up the auth together with the group and overcoming the blockers with the help of team.

June 28, 2023:

Today i worked on -

setting up the issues for pantries check-ins
learning about the databases

June 27, 2023:

Today i worked on -

setting up the issues for fridge-listings
discussion with the team on how to go over the issues
project-setup
