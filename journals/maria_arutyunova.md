6/28/23

Worked on getting all of our git lab issues into gitlab. Worked on making our journals. Started doing the project based on the Learn "Project Advice".

Tomorrow we will work on the Auth and Database stuff together with share screens so we can all be on the same page and off to a good start.

6/29/23

Today we finished setting up the database and pgAdmin as a team. We made a merge request for all of the work we completed today. We got pretty far with the AUTH stuff as a group but ended up getting an error so we are going to try to research more tonight and then work as a team tomorrow to get that all finished as well as getting that pushed into main.

6/30/23

Today we finished doing the AUTH stuff and getting another merge request done. We went ahead and all pulled down the file to have on our computers so we could get started on individual backend points.

7/3/23 - 7/7/23

We had our fourth of July break over this week. We all worked on getting started on our endpoints and making sure we had at least something to work on once we got back. I worked on the pantry endpoints but couldn't test anything until I got my other partners code for neighborhoods.

7/10/2023

We got back from our too short break! We got a lot done today. We did some merge requests today from two people. Then I went ahead and merged and pulled everything into my pantry branch so I could start testing out the endpoints that I made over the break. I fixed everything that wasn't working with the help of my teammates. I also set up the auth points for update, delete and create a pantry. I finished everything and commited to my local branch to do a merge request tomorrow.

7/11/23

We spent some more time as a team making merge requests and getting everything set up. I got everything set up for pantry requests. But am having an issue for the GET method so I will try to solve that tomorrow.

7/12/23

Spent today working on end points and creating merge requests. I finished all of my backend endpoints which were pantry requests and pantries. I fixed the erorr that I had with pantry requests for the GET method. Since I finished the GET and POST for requests I got that merged into main as well. We are going to try getting setup for the frontend AUTH stuff tomorrow. As well as go over some more frontend stuff and try to get that started tomorrow.

7/13/23

Spent today figuring out which two libraries we were going to use for our frontend. We also finished up working on endpoints and hopefully tomorrow all of our API backend endpoints will be complete. Tomorrow we are going to work on getting our login,signup pages up and running as a group. We are also going to set up which tables, buttons, etc for styling that we agree on so that we can have a cohesive project. Tomorrow we will also be putting all of our issues for the front-end into Gitlab.

7/17/23

Worked on setting up our front-end Auth today! Had some trouble but got some help from Caleb to be able to work through the bugs. The main issue was that the already built in library was causing the Post and Get requests to try to pull from localhost 3000 instead of 8000. The only fix we were able to find (besides changing our whole backend from email to username) was to copy the library into a seperate file and change things in there manually so that it would stop pulling from the library that downloads itself automatically due to the txt.requirments file. We also added our styling to a seperate page and used some of the styles that we selected for our front-end login and signup pages. Tomorrow I think we will keep working on the other front-end pages.

7/18/23

Spent today working on front-end pages. I was able to complete the LoginForm page with some help from Sam with my gradient styling. I was also able to complete the PantryDetail and NeighborhoodList page. I did have some trouble with the neighborhood list because of the postal codes but it did work in the end by looking for neighborhoods and postal codes associated with each other. Tomorrow I am going to work on my last front-end page which is the Neighborhood Form (create a neighborhood). I am also going to go ahead and maybe work on the Nav.js and ask people what they want to do about the Main or Homepage .js file.

7/19/23

Worked on finishing up my last two react front-end pages today. The neighborhood form gave me some trouble but I was able to complete it. Tomorrow we will continue working on front-end pages as well as writing unit tests.

7/20/23

Today I worked on my unit tests. I also refactored all of my back-end code to match fridges better. I also need to redue my pantry detail page but I am waiting for Sam to finish his up so again we can have a more cohesive final product. Plus my page broke after my refactoring so either way it needs to be redone. I will either finish my last front-end page today or over the weekend. We will also be doing deployment on Monday because we don't think we will have enough time to do it today and will work on it as a group.

7/24/23

We had some progress over the weekend and good communication about fixing/updating and creating certain React front-end pages. We also created our Nav branch (thank you Jahaziel). Today we did not get a chance to work on deployment because we had our practice test and we also had so many merge requests to do from over the weekend. We helped each other with a couple pages and then I was finally able to get my pantry detail back up and running but haven't merged it in yet. I also created another backend endpoint to do what Sam did with his Fridge Detail to my Pantry Detail. I still have some kinks to work out but will focus on that tomorrow with Sam. We will forsure work on deployment tomorrow as a group and I think after that we only have 1 more front-end page left.

7/25/23

We spent majority of the day working on Deployment today. We finished our React Front end pages and then began working on deployment which took us the full whole day. We didn't think it would take so long but glad we got our site deployed! We did have some issues that we fixed. We also have a couple more to fix (promise errors) but after that, we should have a deployed fully functional website. :D
