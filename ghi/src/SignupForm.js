import { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import { useNavigate, Link } from "react-router-dom";

const SignupForm = () => {
  const [password, setPassword] = useState("");
  const [first_name, setFirstName] = useState("");
  const [pronouns, setPronouns] = useState("");
  const [email, setEmail] = useState("");
  const title = "Sign Up";
  const { register } = useAToken();
  const navigate = useNavigate();

  const handleRegistration = (e) => {
    e.preventDefault();
    const accountData = {
      password: password,
      first_name: first_name,
      pronouns: pronouns,
      email: email,
      type: 1,
    };
    register(accountData, `${process.env.REACT_APP_API_HOST}/api/users/`);
    e.target.reset();
    navigate("/fridges");
  };

  useEffect(() => {
    document.title = title;
  }, []);

  return (
    <section className="bg-white relative flex h-screen w-screen flex-col">
      <div className="lg:grid lg:min-h-screen lg:grid-cols-12">
        <section className="relative flex h-32 items-end bg-gradient-to-tr from-pink-500 via-red-500 to-yellow-500 lg:col-span-5 lg:h-full xl:col-span-6">
          <div className="hidden lg:relative lg:block lg:p-12">
            <h2 className="mt-6 text-2xl font-bold text-white sm:text-3xl md:text-4xl">
              Sign Up
            </h2>
            <p className="mt-4 leading-relaxed text-white">
              Please enter your details.
            </p>
          </div>
        </section>

        <main className="flex items-center justify-center px-8 py-8 sm:px-12 lg:col-span-7 lg:px-16 lg:py-12 xl:col-span-6">
          <div className="max-w-xl lg:max-w-3xl">
            <div className="relative mt-8 block lg:hidden">
              <h1 className="mt-2 text-2xl font-bold text-gray-900 sm:text-3xl md:text-4xl">
                Sign Up
              </h1>

              <p className="mt-4 leading-relaxed text-gray-500">
                Please enter your details.
              </p>
            </div>

            <form
              className="mt-8 grid grid-cols-6 gap-6"
              onSubmit={(e) => handleRegistration(e)}
            >
              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="FirstName"
                  className="block text-sm font-medium text-gray-700"
                >
                  First Name
                </label>
                <input
                  name="first_name"
                  type="text"
                  id="FirstName"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  onChange={(e) => {
                    setFirstName(e.target.value);
                  }}
                />
              </div>

              <div className="col-span-6 sm:col-span-3">
                <label
                  htmlFor="Pronouns"
                  className="block text-sm font-medium text-gray-700"
                >
                  Pronouns
                </label>
                <input
                  name="pronouns"
                  id="Pronouns"
                  type="text"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  onChange={(e) => {
                    setPronouns(e.target.value);
                  }}
                />
              </div>
              <div className="col-span-6">
                <label
                  htmlFor="Email"
                  className="block text-sm font-medium text-gray-700"
                >
                  Email (this will be your username)
                </label>
                <input
                  name="email"
                  type="email"
                  id="Email"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
              </div>

              <div className="col-span-6">
                <label
                  htmlFor="Password"
                  className="block text-sm font-medium text-gray-700"
                >
                  Password
                </label>
                <input
                  name="password"
                  type="password"
                  id="Password"
                  className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                />
              </div>

              <div className="col-span-6">
                <p className="text-sm text-gray-500">
                  By creating an account, you agree to our{" "}
                  <a
                    href="https://docs.google.com/document/d/1juG52TDyB_aNBIJHGrACUR3IQxyFAJ-G52c2JZvUVJQ/edit"
                    className="text-gray-700 underline"
                  >
                    terms and conditions
                  </a>
                  .
                </p>
              </div>

              <div className="col-span-6 sm:flex sm:items-center sm:gap-4">
                <button
                  type="submit"
                  className="w-full inline-block text-center rounded-full bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500 p-[2px] hover:text-white focus:outline-none focus:ring active:text-opacity-75"
                >
                  <span className="block rounded-full bg-white px-8 py-3 text-sm font-medium hover:bg-transparent">
                    Create an account
                  </span>
                </button>

                <p className="mt-4 text-sm text-gray-500 sm:mt-0">
                  Already have an account?{" "}
                  <Link to="/login" className="text-gray-700 underline">
                    Log in
                  </Link>
                  .
                </p>
              </div>
            </form>
          </div>
        </main>
      </div>
    </section>
  );
};

export default SignupForm;
