import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import useAToken from "./UIAuth.tsx";
import { fetchData } from "./utils/fetchdata.js";

export default function ListPantries({ convertTimeFormat }) {
  const [pantries, setPantries] = useState([]);
  const { token } = useAToken();
  const title = "List of Pantries";

  useEffect(() => {
    fetchData("/pantries", setPantries);
    document.title = title;
  }, []);

  return (
    <div className="p-6">
      <div className="flex justify-between items-center p-6">
        <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
          <span className="text-transparent bg-clip-text bg-gradient-to-r from-teal-500 via-purple-500 to-green-500">
            Community Pantries
          </span>
        </h1>
        {token && (
          <Link to="/pantries/new">
            <button className="inline-block shrink-0 rounded-md border border-purple-600 bg-purple-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-transparent hover:text-purple-600 focus:outline-none focus:ring active:text-blue-500">
              Add a Pantry
            </button>
          </Link>
        )}
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-12 ">
        {pantries.map((pantry) => {
          return (
            <div
              className="ease-in-out delay-150 duration-300 h-auto max-w-full rounded-lg shadow-lg hover:shadow-xl ring-1 ring-gray-900/5 hover:ring-2 hover:ring-indigo-700 hover:shadow-indigo-200"
              key={pantry.id}
            >
              <Link
                to={`/pantries/${pantry.id}`}
                className="block rounded-lg p-4"
              >
                <img
                  alt={pantry.pantry_name}
                  src={pantry.pantry_picture}
                  className="h-56 w-full rounded-md object-cover"
                />
                <div className="mt-2 text-center">
                  <dl>
                    <div>
                      <dt className="sr-only">Pantry Name</dt>
                      <dd className="p-2 font-medium">{pantry.pantry_name}</dd>
                    </div>

                    <div>
                      <dt className="sr-only">Neighborhood</dt>
                      <dd className="bg-green-100 text-green-800 text-s m-auto w-40 font-medium px-2.5 py-0.5 rounded dark:bg-gray-700 dark:text-green-400 border border-green-400">
                        {pantry.neighborhood.neighborhood_name}
                      </dd>
                    </div>

                    <div>
                      <dt className="sr-only">Location</dt>
                      <dd className="p-2 select-all">{pantry.location}</dd>
                    </div>

                    <div>
                      <dt className="sr-only">Hours</dt>
                      <dd>
                        Opens:{" "}
                        <time dateTime={`${pantry.hours_start}`}>
                          {convertTimeFormat(pantry.hours_start)}
                        </time>
                      </dd>
                      <dd>
                        Closes:{" "}
                        <time dateTime={`${pantry.hours_end}`}>
                          {convertTimeFormat(pantry.hours_end)}
                        </time>
                      </dd>
                    </div>
                  </dl>
                </div>
                <div className="mt-6 flex items-center justify-center gap-8 text-xs">
                  <div className="sm:inline-flex sm:shrink-0 sm:items-center sm:gap-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="h-5 w-5 text-indigo-700"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z"
                      />
                    </svg>

                    <div className="mt-1.5 sm:mt-0">
                      <p className="text-gray-500">Status</p>
                      <p className="font-medium">
                        {pantry.active ? "Active" : "Inactive"}
                      </p>
                    </div>
                  </div>

                  <div className="sm:inline-flex sm:shrink-0 sm:items-center sm:gap-2">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="h-5 w-5 text-indigo-700"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5"
                      />
                    </svg>

                    <div className="mt-1.5 sm:mt-0">
                      <p className="text-gray-500">Date Created</p>
                      <p className="font-medium">
                        {new Date(pantry.date_created).toDateString()}{" "}
                      </p>
                    </div>
                  </div>
                </div>
              </Link>
            </div>
          );
        })}
      </div>
    </div>
  );
}
