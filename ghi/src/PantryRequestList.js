import { Fragment } from "react";
import React, { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import { fetchData } from "./utils/fetchdata.js";
import ErrorNotification from "./ErrorNotification.js";

export default function PantryRequestList() {
  const [requests, setRequests] = useState([]);
  const [itemEntries, setItemEntries] = useState([]);
  const [pantries, setPantries] = useState([]);
  const { fetchWithToken } = useAToken();
  const [title] = useState("Pantry Requests");

  async function getRequestData() {
    const requestsURL = `${process.env.REACT_APP_API_HOST}/pantry-requests/`;
    let response = await fetchWithToken(requestsURL);
    let data = await response[0];
    let status = await response[1];

    if (status === true) {
      setRequests(data);
    } else {
      ErrorNotification(response);
    }
  }

  const getPantryName = (requestPantryID) => {
    const pantry = pantries.find((p) => p.id === requestPantryID);
    return pantry.pantry_name;
  };

  const getPantryAddress = (requestPantryID) => {
    const pantry = pantries.find((p) => p.id === requestPantryID);
    return pantry.location;
  };

  const getItemDetails = (requestItems) => {
    const names = [];
    if (requests.length > 0) {
      for (let good of requestItems) {
        const result = itemEntries.find((item) => item.id === good);
        names.push(result.name);
      }
    }
    return names;
  };

  useEffect(() => {
    document.title = title;
    getRequestData();
    fetchData("/items/", setItemEntries);
    fetchData("/pantries", setPantries);
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <section className="container px-4 mx-auto">
        <div className="flex items-center gap-x-3">
          <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
              Pantry Requests
            </span>
          </h1>

          <span className="px-3 py-1 text-xs text-orange-600 bg-orange-100 rounded-full dark:bg-gray-800 dark:text-blue-400">
            {requests.length}
          </span>
        </div>
        <div className="flex flex-col mt-6">
          <div className="mx-4 my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div className="overflow-hidden border border-slate-700 dark:border-gray-700 md:rounded-lg">
                <table className="w-full divide-y divide-slate-700 dark:divide-gray-700">
                  <thead className="bg-white dark:bg-black-800">
                    <tr key="tablehead">
                      <th
                        scope="col"
                        className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-slate-700 dark:text-gray-400"
                      >
                        <div className="flex items-center gap-x-3">
                          <input
                            type="checkbox"
                            className="text-rose-600 border-black-300 focus:ring-rose-600 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                          ></input>
                          <span>PANTRY</span>
                        </div>
                      </th>
                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        <button className="flex items-center gap-x-2">
                          <span>DATE</span>
                        </button>
                      </th>
                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        ITEMS REQUESTED
                      </th>
                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        COMMENTS
                      </th>
                      <th
                        scope="col"
                        className="px-4 py-3.5 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400"
                      >
                        VISITOR
                      </th>

                      <th scope="col" className="relative py-3.5 px-4">
                        <span className="sr-only">Edit</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                    {requests.map((request) => {
                      return (
                        <Fragment key={`request` + request.id}>
                          <tr>
                            <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                              <div className="inline-flex items-center gap-x-3">
                                <input
                                  type="checkbox"
                                  className="text-rose-600 border-black-300 focus:ring-rose-600 rounded dark:bg-gray-900 dark:ring-offset-gray-900 dark:border-gray-700"
                                ></input>
                                <div className="flex items-center gap-x-2">
                                  <div>
                                    <h2 className="font-medium text-gray-800 dark:text-white ">
                                      {getPantryName(request.pantry)}
                                    </h2>
                                    <p className="text-sm font-normal text-gray-600 dark:text-gray-400">
                                      {getPantryAddress(request.pantry)}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                              {request.date_created}
                            </td>
                            <td className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap">
                              <div className="inline-flex items-center gap-x-1">
                                {getItemDetails(request.items).map(
                                  (cat, index) => {
                                    return (
                                      <span
                                        key={`item` + index}
                                        className="inline-flex items-center justify-center rounded-full bg-rose-600 px-2.5 py-0.5 text-white"
                                      >
                                        {cat}
                                      </span>
                                    );
                                  }
                                )}
                              </div>
                            </td>
                            <td
                              key={`additional` + request.id}
                              className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap"
                            >
                              {request.additional}
                            </td>
                            <td
                              key={`visitor` + request.id}
                              className="px-4 py-4 text-sm text-gray-500 dark:text-gray-300 whitespace-nowrap"
                            >
                              {request.visitor}
                            </td>
                            <td
                              key={`edit` + request.id}
                              className="px-4 py-4 text-sm whitespace-nowrap"
                            >
                              <div className="flex items-center gap-x-6">
                                <button className="text-gray-500 transition-colors duration-200 dark:hover:text-red-500 dark:text-gray-300 hover:text-red-500 focus:outline-none">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth="1.5"
                                    stroke="currentColor"
                                    className="w-5 h-5"
                                  >
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                    />
                                  </svg>
                                </button>

                                <button className="text-gray-500 transition-colors duration-200 dark:hover:text-yellow-500 dark:text-gray-300 hover:text-yellow-500 focus:outline-none">
                                  <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                    strokeWidth="1.5"
                                    stroke="currentColor"
                                    className="w-5 h-5"
                                  >
                                    <path
                                      strokeLinecap="round"
                                      strokeLinejoin="round"
                                      d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                                    />
                                  </svg>
                                </button>
                              </div>
                            </td>
                          </tr>
                        </Fragment>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
