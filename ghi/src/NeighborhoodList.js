import React, { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import Lottie from "lottie-react";
import forest from "./forest.json";

const NeighborhoodList = () => {
  const { token } = useAToken();
  const [postalCodes, setPostalCodes] = useState([]);
  const [neighborhoods, setNeighborhoods] = useState([]);
  const title = "List of Neighborhoods";

  const fetchPostalCodes = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/postal_codes`
      );
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setPostalCodes(data);
    } catch (err) {
      console.error("Failed to fetch postal codes:", err);
    }
  };

  const fetchNeighborhoods = async () => {
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API_HOST}/neighborhoods`
      );
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setNeighborhoods(data);
    } catch (err) {
      console.error("Failed to fetch neighborhoods:", err);
    }
  };

  useEffect(() => {
    fetchPostalCodes();
    fetchNeighborhoods();
    document.title = title;
  }, []);

  const neighborhoodPostalCodes = neighborhoods.reduce(
    (result, neighborhood) => {
      const codes = postalCodes
        .filter((code) => code.neighborhood_id === neighborhood.id)
        .map((code) => code.postal_code);
      if (result[neighborhood.neighborhood_name]) {
        result[neighborhood.neighborhood_name] = [
          ...result[neighborhood.neighborhood_name],
          ...codes,
        ];
      } else {
        result[neighborhood.neighborhood_name] = codes;
      }
      return result;
    },
    {}
  );

  return (
    <div className="flex h-screen w-screen flex-col justify-start overflow-hidden">
      <div className="mt-16 mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <div>
          <h1 className="text-center text-2xl font-bold sm:text-3xl gradient-text">
            Neighborhoods
          </h1>
        </div>
        <div className="mx-auto max-w-lg">
          <div className="relative bg-white px-6 pb-8 shadow-xl ring-1 ring-gray-900/5 sm:mx-auto sm:max-w-lg sm:rounded-lg sm:px-10 mt-4">
            <table className="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
              <thead className="bg-gray-50 dark:bg-gray-800">
                <tr>
                  <th className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    Neighborhood
                  </th>
                  <th className="py-3.5 px-4 text-sm font-normal text-left rtl:text-right text-gray-500 dark:text-gray-400">
                    Postal Codes
                  </th>
                  {token && <th className="px-6 py-3 bg-gray-50"></th>}
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200 dark:divide-gray-700 dark:bg-gray-900">
                {Object.entries(neighborhoodPostalCodes).map(
                  ([neighborhood, codes]) => {
                    return (
                      <tr key={neighborhood}>
                        <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                          <div className="inline-flex items-center gap-x-3">
                            {neighborhood}
                          </div>
                        </td>
                        <td className="px-4 py-4 text-sm font-medium text-gray-700 whitespace-nowrap">
                          <div className="inline-flex items-center gap-x-3">
                            {codes.map((code, index) => (
                              <span
                                key={index}
                                className="inline-flex items-center justify-center rounded-full bg-green-100 px-2.5 py-0.5 text-green-700"
                              >
                                <p className="whitespace-nowrap text-sm">
                                  {code}
                                </p>
                              </span>
                            ))}
                          </div>
                        </td>
                      </tr>
                    );
                  }
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="absolute bottom-0 left-0 right-0 overflow-hidden">
        <div className="flex justify-around">
          <Lottie
            animationData={forest}
            style={{
              height: "30%",
              width: "33.33%",
              zIndex: -1,
            }}
            loop={false}
          />
          <Lottie
            className="-ml-1"
            animationData={forest}
            style={{
              height: "30%",
              width: "33.33%",
              zIndex: -1,
            }}
            loop={false}
          />
          <Lottie
            className="-ml-1"
            animationData={forest}
            style={{
              height: "30%",
              width: "33.33%",
              zIndex: -1,
            }}
            loop={false}
          />
        </div>
      </div>
    </div>
  );
};

export default NeighborhoodList;
