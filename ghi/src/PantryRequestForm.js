import { useState, useEffect } from "react";
import { fetchData } from "./utils/fetchdata";
import { handleChange } from "./utils/handleChange";
import { handleChecked } from "./utils/handleChecked";

export default function PantryRequestForm() {
  const [pantries, setPantries] = useState([]);

  const [toggle, setToggle] = useState(true);

  const [pantry, setPantry] = useState();
  const [itemsList, setItemsList] = useState([]);
  const [itemsSelected, setItemsSelected] = useState([]);
  const [additional, setAdditional] = useState("");
  const [visitor, setVisitor] = useState("");

  const title = "Pantry Request Form";

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.pantry = Number(pantry);
    data.items = itemsSelected;
    data.additional = additional;
    data.visitor = visitor;
    const pantryRequestUrl = `${process.env.REACT_APP_API_HOST}/pantry-requests/`;
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const pantryRequestResponse = await fetch(pantryRequestUrl, fetchOptions);
    if (pantryRequestResponse.ok) {
      setPantry("");
      setItemsSelected([]);
      setAdditional("");
      setVisitor("");
      setToggle(true);
    }
  };

  useEffect(() => {
    fetchData("/pantries", setPantries);
    fetchData("/items/", setItemsList);
    document.title = title;
  }, []);

  return (
    <section className="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800">
      <h2 className="text-lg font-semibold text-gray-700 capitalize dark:text-white">
        pantry Item Request
      </h2>
      <form onSubmit={handleSubmit} name="pantryCheckInForm">
        {/* pantry Dropdown */}
        <div>
          <label htmlFor="pantry" className="text-gray-700 dark:text-gray-200">
            pantry
          </label>

          <select
            name="pantry"
            id="pantry"
            className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
            onChange={(event) => {
              handleChange(event, setPantry);
            }}
            value={pantry}
          >
            <option value="">Please select</option>
            {pantries.map((pantry) => {
              return (
                <option key={pantry.id} value={Number(pantry.id)}>
                  {pantry.pantry_name}
                </option>
              );
            })}
          </select>
        </div>
        {/* Items select */}
        <div>
          <label
            className="text-gray-700 dark:text-gray-200"
            htmlFor="stockedItems"
          >
            What items would you like to see at this location in the future?
          </label>
          <br></br>
          <br></br>
          <button
            id="ItemsRequested"
            onClick={() => setToggle(!toggle)}
            className="inline-flex items-center px-4 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
            type="button"
          >
            Check all that apply:{" "}
            <svg
              className="w-2.5 h-2.5 ml-2.5"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 10 6"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m1 1 4 4 4-4"
              />
            </svg>
          </button>
          {!toggle && (
            <ul
              className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700 dark:text-gray-200"
              aria-labelledby="dropdownSearchButton"
            >
              {itemsList.map((item) => {
                return (
                  <li key={"itemLi" + item.id}>
                    <div
                      key={"itemDiv" + item.id}
                      className="flex items-center p-2 rounded hover:bg-gray-100 dark:hover:bg-gray-600"
                    >
                      <input
                        id={item.id}
                        onChange={(event) => {
                          handleChecked(event, setItemsSelected, itemsSelected);
                        }}
                        type="checkbox"
                        value={Number(item.id)}
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                      />
                      <label
                        key={"itemLabel" + item.id}
                        htmlFor="checkbox-item-11"
                        className="w-full ml-2 text-sm font-medium text-gray-900 rounded dark:text-gray-300"
                      >
                        {item.name}
                      </label>
                    </div>
                  </li>
                );
              })}
            </ul>
          )}
        </div>
        {/* Additional */}
        <div>
          <label
            className="text-gray-700 dark:text-gray-200"
            htmlFor="additional"
          >
            Any other comments?
          </label>
          <input
            id="additional"
            type="text"
            className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
            onChange={(event) => {
              handleChange(event, setAdditional);
            }}
            value={additional}
          />
        </div>
        {/* Visitor */}
        <div>
          <label className="text-gray-700 dark:text-gray-200" htmlFor="visitor">
            Name or initials?
          </label>
          <input
            id="visitor"
            type="text"
            className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
            onChange={(event) => {
              handleChange(event, setVisitor);
            }}
            value={visitor}
          />
        </div>
        {/* Save Button */}
        <div className="flex justify-center mt-6">
          <button className="px-8 py-2.5 leading-5 text-white transition-colors duration-300 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-600">
            Save
          </button>
        </div>
      </form>
    </section>
  );
}
