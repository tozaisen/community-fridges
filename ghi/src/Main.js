import { Link } from "react-router-dom";
import Lottie from "lottie-react";
import veggies from "./veggies.json";
import fridgesAnimation from "./fridgesAnimation.json";
import { useEffect } from "react";

export default function Main() {
  const title = "Community Food";

  useEffect(() => {
    document.title = title;
  }, []);

  return (
    <section className="bg-white dark:bg-gray-900 ">
      <div className="p-6">
        <div className="flex justify-around items-center p-6">
          <h1 className="mt-8 mb-2 text-center text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-teal-500 via-purple-500 to-green-500">
              Welcome to Community Food
            </span>
          </h1>
        </div>
        <div className="flex rounded-lg place-content-center p-6 mb-8 bg-purple-700">
          <span className="text-center text-white text-xl lg:mx-20 sm:mx-10">
            Community food is a directory of community fridges and pantries
            throughout the world, as well as the organizing tools for the
            volunteer networks that host them.
          </span>
        </div>

        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-12 ">
          <div className="ease-in-out delay-150 duration-300 h-auto max-w-full rounded-lg shadow-lg hover:shadow-xl ring-1 ring-gray-900/5 hover:ring-2 hover:ring-indigo-700 hover:shadow-indigo-200">
            <Link to={`/pantries`} className="block p-4">
              <h1 className="mt-8 mb-8  text-center text-l font-extrabold text-gray-900 dark:text-white sm:text-xl md:text-2xl lg:text-3xl">
                <span className="text-teal-600">View Pantries</span>
              </h1>
              <Lottie
                animationData={veggies}
                style={{
                  height: "25%",
                  zIndex: -1,
                  overflow: "hidden",
                  position: "static",
                }}
                loop={true}
              />
            </Link>
          </div>
          <div className="ease-in-out delay-150 duration-300 h-auto max-w-full rounded-lg shadow-lg hover:shadow-xl ring-1 ring-gray-900/5 hover:ring-2 hover:ring-indigo-700 hover:shadow-indigo-200">
            <Link to={`/fridges`} className="block p-4">
              <h1 className="mt-8 mb-8 text-center font-extrabold text-gray-900 dark:text-white sm:text-xl md:text-2xl lg:text-3xl">
                <span className="text-teal-600">View Fridges</span>
              </h1>
              <Lottie
                animationData={fridgesAnimation}
                style={{
                  height: "25%",
                  zIndex: -1,
                  overflow: "hidden",
                  position: "static",
                }}
                loop={true}
              />
            </Link>
          </div>
        </div>
        <div className="flex justify-around p-6 mt-8 mb-8 font-extrabold text-gray-900 dark:text-white md:text-2xl lg:text-3xl">
          <div className="md:gap-8 sm:flex sm:gap-4">
            <Link to="/login" className="">
              <button className="w-full inline-block text-center mb-4 rounded-full bg-gradient-to-r from-teal-500 via-purple-500 to-green-500 p-[2px] hover:text-white focus:outline-none focus:ring active:text-opacity-75">
                <span className="block rounded-full bg-white px-8 py-3 text-lg font-medium hover:bg-transparent">
                  Already a volunteer? Login!
                </span>
              </button>
            </Link>
            <Link to="/signup">
              <button className="w-full inline-block text-center rounded-full bg-gradient-to-r from-teal-500 via-purple-500 to-green-500 p-[2px] hover:text-white focus:outline-none focus:ring active:text-opacity-75">
                <span className="block rounded-full bg-white px-8 py-3 text-lg font-medium hover:bg-transparent">
                  Want to be a volunteer? Sign up!
                </span>
              </button>
            </Link>
          </div>
        </div>
        <figure className="max-w-screen-md mx-auto text-center mb-8">
          <svg
            className="w-10 h-10 mx-auto mb-3 text-gray-400 dark:text-gray-600"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="currentColor"
            viewBox="0 0 18 14"
          >
            <path d="M6 0H2a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4v1a3 3 0 0 1-3 3H2a1 1 0 0 0 0 2h1a5.006 5.006 0 0 0 5-5V2a2 2 0 0 0-2-2Zm10 0h-4a2 2 0 0 0-2 2v4a2 2 0 0 0 2 2h4v1a3 3 0 0 1-3 3h-1a1 1 0 0 0 0 2h1a5.006 5.006 0 0 0 5-5V2a2 2 0 0 0-2-2Z" />
          </svg>
          <blockquote>
            <p className="mb-4 text-xl italic font-medium text-gray-900 dark:text-white">
              The refrigerators — some of which also have pantries close by —
              offer free food to anyone who needs it. Launched in July of 2020,
              the program has expanded over the past two years into an example
              of what can happen when neighbors come together.
            </p>
            <p className="text-xl italic font-medium text-gray-900 dark:text-white">
              The goal is to cut down on food waste by letting people share
              excess food — maybe from a restaurant, work event or bountiful
              backyard vegetable garden — with people in need.
            </p>
          </blockquote>
          <figcaption className="flex items-center justify-center mt-6 space-x-3">
            <img
              className="w-6 h-6 rounded-full"
              src="https://variety.com/wp-content/uploads/2023/01/LAist-e1675186823794.png"
              alt="LAist logo"
            />
            <div className="flex items-center divide-x-2 divide-gray-500 dark:divide-gray-700">
              <cite className="pr-3 font-medium text-gray-900 dark:text-white">
                - LAist.com
              </cite>
              <cite className="pl-3 text-sm text-gray-500 dark:text-gray-400">
                August 22, 2022
              </cite>
            </div>
          </figcaption>
        </figure>
      </div>
    </section>
  );
}
