import { useState, useEffect } from "react";
import { fetchData } from "./utils/fetchdata";
import { handleChange } from "./utils/handleChange";
import { handleChecked } from "./utils/handleChecked";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function FridgeCheckin() {
  const [toggle, setToggle] = useState(true);
  const [reasons, setReasons] = useState([]);
  const [fridges, setFridges] = useState([]);
  const [cleanStatuses, setCleanStatuses] = useState([]);
  const [foodStatuses, setFoodStatuses] = useState([]);
  const [itemsList, setItemsList] = useState([]);

  const navigate = useNavigate();

  const [fridge, setFridge] = useState();
  const [reasonsSelected, setReasonsSelected] = useState([]);
  const [cleanStatus, setCleanStatus] = useState();
  const [foodStatus, setFoodStatus] = useState();
  const [stockedItems, setStockedItems] = useState([]);
  const [picture, setPicture] = useState("");
  const [additional, setAdditional] = useState("");
  const [visitor, setVisitor] = useState("");
  const [queryParameters] = useSearchParams();
  const title = "Fridge Check-In Form";

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.fridge = Number(fridge);
    data.reasons = reasonsSelected;
    data.clean_status = Number(cleanStatus);
    data.food_status = Number(foodStatus);
    data.stocked_items = stockedItems;
    data.picture = picture;
    data.additional = additional;
    data.visitor = visitor;

    const fridgeCheckinUrl = `${process.env.REACT_APP_API_HOST}/api/fridgecheckins`;
    const fetchOptions = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const fridgeCheckinResponse = await fetch(fridgeCheckinUrl, fetchOptions);

    if (fridgeCheckinResponse.ok) {
      navigate(`/fridges/${data.fridge}`);
    }
  };

  useEffect(() => {
    const fridge_id = queryParameters.get("fridgeid");

    if (fridge_id) {
      setFridge(Number(fridge_id));
    } else {
      setFridge("");
    }
  }, [queryParameters]);

  useEffect(() => {
    fetchData("/api/reasons", setReasons);
    fetchData("/fridges", setFridges);
    fetchData("/clean_statuses", setCleanStatuses);
    fetchData("/food_statuses", setFoodStatuses);
    fetchData("/items/", setItemsList);
    document.title = title;
  }, []);

  return (
    <section className="container px-4 mx-auto">
      <div className="flex items-center gap-x-3">
        <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
          <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
            Fridge Check-In
          </span>
        </h1>
      </div>

      <form onSubmit={handleSubmit} name="FridgeCheckInForm">
        <div className="grid grid-cols-1 gap-6 mt-4">
          {/* Fridge Dropdown */}
          <div>
            <label
              htmlFor="Fridge"
              className="text-gray-700 dark:text-gray-200"
            >
              Fridge
            </label>

            <select
              name="Fridge"
              id="Fridge"
              className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setFridge);
              }}
              value={fridge}
            >
              <option value="">Please select</option>
              {fridges.map((fridge) => {
                return (
                  <option key={fridge.id} value={Number(fridge.id)}>
                    {fridge.fridge_name}
                  </option>
                );
              })}
            </select>
          </div>
          {/* Reasons Select */}
          <div>
            <h3 className="mb-4 font-semibold text-gray-900 dark:text-white">
              What brings you to the fridge today?
            </h3>
            <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white">
              {reasons.map((reason) => {
                return (
                  <li
                    className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r dark:border-gray-600"
                    key={"reasonLi" + reason.id}
                  >
                    <div
                      className="flex items-center pl-3"
                      key={"reasonDiv" + reason.id}
                    >
                      <input
                        key={reason.id}
                        id={reason.id}
                        onChange={(event) => {
                          handleChecked(
                            event,
                            setReasonsSelected,
                            reasonsSelected
                          );
                        }}
                        type="checkbox"
                        value={Number(reason.id)}
                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                      />
                      <label
                        key={"reasonLabel" + reason.id}
                        htmlFor={reason.id}
                        className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                      >
                        {reason.name}
                      </label>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
          {/* Clean Status */}
          <div>
            <label
              htmlFor="cleanStatus"
              className="text-gray-700 dark:text-gray-200"
            >
              How clean is the fridge?
            </label>

            <select
              name="cleanStatus"
              id="cleanStatus"
              className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setCleanStatus);
              }}
              value={cleanStatus}
            >
              <option value="">Please select</option>
              {cleanStatuses.map((status) => {
                return (
                  <option key={status.id} value={Number(status.id)}>
                    {status.clean_status_name}
                  </option>
                );
              })}
            </select>
          </div>
          {/* Food Status */}
          <div>
            <label
              htmlFor="Food Status"
              className="text-gray-700 dark:text-gray-200"
            >
              How much food is in the fridge?
            </label>
            <select
              name="Food Status"
              id="Food Status"
              className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setFoodStatus);
              }}
              value={foodStatus}
            >
              <option value="">Please select</option>
              {foodStatuses.map((status) => {
                return (
                  <option key={status.id} value={Number(status.id)}>
                    {status.food_status_name}
                  </option>
                );
              })}
            </select>
          </div>
          {/* Stocked Item */}
          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="stockedItems"
            >
              Which items are stocked in the fridge today?
            </label>
            <br></br>
            <br></br>
            <button
              id="stockedItems"
              onClick={() => setToggle(!toggle)}
              className="mt-4 text-white focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500"
              type="button"
            >
              Check all that apply:{" "}
              <svg
                className="w-2.5 h-2.5 ml-2.5"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 10 6"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 1 4 4 4-4"
                />
              </svg>
            </button>
            {!toggle && (
              <ul
                className="h-48 px-3 pb-3 overflow-y-auto text-sm text-gray-700 dark:text-gray-200"
                aria-labelledby="dropdownSearchButton"
              >
                {itemsList.map((item) => {
                  return (
                    <li key={"itemLi" + item.id}>
                      <div
                        key={"itemDiv" + item.id}
                        className="flex items-center p-2 rounded hover:bg-gray-100 dark:hover:bg-gray-600"
                      >
                        <input
                          id={item.id}
                          onChange={(event) => {
                            handleChecked(event, setStockedItems, stockedItems);
                          }}
                          type="checkbox"
                          value={Number(item.id)}
                          className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                        />
                        <label
                          key={"itemLabel" + item.id}
                          htmlFor="checkbox-item-11"
                          className="w-full ml-2 text-sm font-medium text-gray-900 rounded dark:text-gray-300"
                        >
                          {item.name}
                        </label>
                      </div>
                    </li>
                  );
                })}
              </ul>
            )}
          </div>
          {/* Picture URL */}
          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="pictureUrl"
            >
              Picture URL - from imgur
            </label>
            <input
              id="pictureURL"
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setPicture);
              }}
              value={picture}
            />
          </div>
          {/* Additional */}
          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="additional"
            >
              Any other comments?
            </label>
            <input
              id="additional"
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setAdditional);
              }}
              value={additional}
            />
          </div>
          {/* Visitor */}
          <div>
            <label
              className="text-gray-700 dark:text-gray-200"
              htmlFor="visitor"
            >
              Name or initials?
            </label>
            <input
              id="visitor"
              type="text"
              className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring"
              onChange={(event) => {
                handleChange(event, setVisitor);
              }}
              value={visitor}
            />
          </div>
          {/* Save Button */}
          <div className="mt-4 mb-4 flex justify-center mt-6">
            <button
              className="w-full inline-block text-center rounded-full bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500 p-[2px] text-white focus:outline-none focus:ring-4 focus:ring-yellow-300 active:text-opacity-75 font-medium"
              type="submit"
            >
              <span className="block rounded-full px-8 py-3 text-sm font-medium hover:text-black hover:bg-transparent">
                Submit
              </span>
            </button>
          </div>
        </div>
      </form>
    </section>
  );
}
