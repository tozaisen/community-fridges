import React, { useEffect, useState } from "react";
import { handleChange } from "./utils/handleChange";
import { handleChecked } from "./utils/handleChecked";
import { useNavigate } from "react-router-dom";

export default function PantryCheckInForm() {
  const [neighborhoods, setNeighborhoods] = useState([]);
  const [selectedNeighborhood, setSelectedNeighborhood] = useState(0);
  const [pantries, setPantries] = useState([]);
  const [foodStatuses, setFoodStatuses] = useState([]);
  const [cleanStatus, setCleanStatus] = useState([]);
  const [photoURL, setPhotoURL] = useState("");
  const [additional, setAdditional] = useState("");
  const [name, setName] = useState("");
  const [reasons, setReasons] = useState([]);
  const [items, setItems] = useState([]);
  const [pantry_id, setPantryID] = useState(0);
  const [food_status, setSelectedFoodStatus] = useState(0);
  const [clean_status, setSelectedCleanStatus] = useState(0);
  const [stocked_items, setSelectedItems] = useState([]);
  const [selected_reasons, setSelectedReasons] = useState([]);
  const [toggle, setToggle] = useState(true);
  const title = "Pantry Check-In Form";

  const navigate = useNavigate();

  const pantryItemsID = [1, 2, 4, 7, 8, 11, 13, 14, 15];
  const filteredItems = items.filter((item) => pantryItemsID.includes(item.id));

  const fetchData = async (address, statemanager) => {
    const url = `${process.env.REACT_APP_API_HOST}${address}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      statemanager(data);
    } else {
      console.error(response);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {
      pantry_id: Number(pantry_id),
      food_status: Number(food_status),
      clean_status: Number(clean_status),
      reasons: selected_reasons,
      stocked_items: stocked_items,
      photo: photoURL,
      Anything_else_details: additional,
      name_or_initials: name,
    };

    const newPantryCheckInURL = `${process.env.REACT_APP_API_HOST}/pantry_check_ins/`;
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(newPantryCheckInURL, fetchConfig);

    if (response.ok) {
      navigate(`/pantries/${pantry_id}`);
    }
  };

  useEffect(() => {
    document.title = title;
    fetchData("/neighborhoods", setNeighborhoods);
    fetchData("/pantries", setPantries);
    fetchData("/food_statuses", setFoodStatuses);
    fetchData("/clean_statuses", setCleanStatus);
    fetchData("/api/reasons", setReasons);
    fetchData("/items/", setItems);
  }, []);

  return (
    <section className="container px-4 mx-auto">
      <form onSubmit={handleSubmit} name="PantryCheckinForm">
        <div className="flex items-center gap-x-3">
          <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
            <span className="text-transparent bg-clip-text bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500">
              Pantry Check-In
            </span>
          </h1>
        </div>
        <div>
          <label
            htmlFor="Neighborhood"
            className="block text-sm font-medium text-gray-900"
          >
            Neighborhood
          </label>

          <select
            required
            name="Neighborhood"
            id="Neighborhood"
            className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm"
            onChange={(event) => {
              handleChange(event, setSelectedNeighborhood);
            }}
          >
            <option value="">Please select</option>
            {neighborhoods.map((hood) => {
              return (
                <option value={hood.id} key={hood.id}>
                  {hood.neighborhood_name}
                </option>
              );
            })}
          </select>
        </div>
        <div>
          <label
            htmlFor="Pantry"
            className="block text-sm font-medium text-gray-900 mt-4"
          >
            Pantry
          </label>

          <select
            required
            name="Pantry"
            id="Pantry"
            className="mt-1.5 w-full rounded-lg border-gray-300 text-gray-700 sm:text-sm"
            onChange={(event) => {
              handleChange(event, setPantryID);
            }}
          >
            <option value="">Please select</option>
            {pantries
              .filter(
                (pantry) =>
                  pantry.neighborhood.id === parseInt(selectedNeighborhood)
              )
              .map((filteredPantry) => (
                <option value={filteredPantry.id} key={filteredPantry.id}>
                  {filteredPantry.pantry_name}
                </option>
              ))}
          </select>
        </div>
        <div>
          <label
            htmlFor="Reasons"
            className="block text-sm font-medium text-gray-900 mt-4 mb-1.5"
          >
            What brings you to the pantry today? Check all that apply.
          </label>
          <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white">
            {reasons.map((reason) => {
              return (
                <React.Fragment key={"reasons" + reason.id}>
                  <li className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r dark:border-gray-600">
                    <div className="flex items-center pl-3">
                      <input
                        id={reason.id}
                        type="checkbox"
                        value={Number(reason.id)}
                        className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                        onChange={(event) => {
                          handleChecked(
                            event,
                            setSelectedReasons,
                            selected_reasons
                          );
                        }}
                      />
                      <label
                        htmlFor={reason.id}
                        className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                      >
                        {reason.name}
                      </label>
                    </div>
                  </li>
                </React.Fragment>
              );
            })}
          </ul>
        </div>
        <div>
          <label
            htmlFor="Cleanliness"
            className="block text-sm font-medium text-gray-900 mt-4 mb-1.5"
          >
            How clean is the pantry? Choose one.
          </label>
          <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white">
            {cleanStatus.map((status) => {
              return (
                <React.Fragment key={"cleanStatus" + status.id}>
                  <li className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r dark:border-gray-600">
                    <div className="flex items-center pl-3">
                      <input
                        id="horizontal-list-radio-license"
                        type="radio"
                        value={status.id}
                        name="cleanStatus"
                        className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                        onChange={(event) => {
                          handleChange(event, setSelectedCleanStatus);
                        }}
                      />
                      <label
                        htmlFor="horizontal-list-radio-license"
                        className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                      >
                        {status.clean_status_name}
                      </label>
                    </div>
                  </li>
                </React.Fragment>
              );
            })}
          </ul>
        </div>
        <div>
          <label
            htmlFor="Stock"
            className="block text-sm font-medium text-gray-900 mt-4 mb-1.5"
          >
            How much food is in the pantry? Choose one.
          </label>
          <ul className="items-center w-full text-sm font-medium text-gray-900 bg-white border border-gray-200 rounded-lg sm:flex dark:bg-gray-700 dark:border-gray-600 dark:text-white">
            {foodStatuses.map((status) => {
              return (
                <React.Fragment key={"foodStatus" + status.id}>
                  <li className="w-full border-b border-gray-200 sm:border-b-0 sm:border-r dark:border-gray-600">
                    <div className="flex items-center pl-3">
                      <input
                        id="horizontal-list-radio-license"
                        type="radio"
                        value={status.id}
                        name="foodStatus"
                        className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                        onChange={(event) => {
                          handleChange(event, setSelectedFoodStatus);
                        }}
                      />
                      <label
                        htmlFor="horizontal-list-radio-license"
                        className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                      >
                        {status.food_status_name}
                      </label>
                    </div>
                  </li>
                </React.Fragment>
              );
            })}
          </ul>
        </div>
        <div>
          <button
            id="dropdownCheckboxButton"
            onClick={() => setToggle(!toggle)}
            // data-dropdown-toggle="dropdownDefaultCheckbox"
            className="mt-4 text-white focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500"
            type="button"
          >
            Which items are stocked in the pantry today?
            <svg
              className="w-2.5 h-2.5 ml-2.5"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 10 6"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m1 1 4 4 4-4"
              />
            </svg>
          </button>
          {!toggle && (
            <ul
              className="p-3 space-y-3 text-sm text-gray-700 dark:text-gray-200"
              aria-labelledby="dropdownCheckboxButton"
            >
              {filteredItems.map((pantryitem) => {
                return (
                  <li key={pantryitem.id}>
                    <div className="flex items-center">
                      <input
                        id={pantryitem.id}
                        type="checkbox"
                        value={Number(pantryitem.id)}
                        className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                        onChange={(event) => {
                          handleChecked(event, setSelectedItems, stocked_items);
                        }}
                      />
                      <label
                        htmlFor="checkbox-item-1"
                        className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                      >
                        {pantryitem.name}
                      </label>
                    </div>
                  </li>
                );
              })}
            </ul>
          )}
        </div>
        <div className="col-span-6 sm:col-span-3">
          <label
            htmlFor="PhotoURL"
            className="mt-4 block text-sm font-medium text-gray-700"
          >
            Photo URL (optional)
          </label>
          <input
            type="text"
            id="PhotoURL"
            value={photoURL}
            className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
            onChange={(event) => {
              handleChange(event, setPhotoURL);
            }}
          />
        </div>
        <div className="col-span-6 sm:col-span-3">
          <label
            htmlFor="PhotoURL"
            className="mt-4 block text-sm font-medium text-gray-700"
          >
            Any additional details? (optional)
          </label>
          <input
            type="text"
            id="additional"
            value={additional}
            className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
            onChange={(event) => {
              handleChange(event, setAdditional);
            }}
          />
        </div>
        <div className="col-span-6 sm:col-span-3">
          <label
            htmlFor="Name"
            className="mt-4 block text-sm font-medium text-gray-700"
          >
            Name or initials (optional)
          </label>
          <input
            type="text"
            id="Name"
            value={name}
            className="mt-1 w-full rounded-md border-gray-200 bg-white text-sm text-gray-700 shadow-sm"
            onChange={(event) => {
              handleChange(event, setName);
            }}
          />
        </div>
        <div className="mt-4 mb-4 col-span-6 sm:flex sm:items-center sm:gap-4">
          <button
            type="submit"
            className="w-full inline-block text-center rounded-full bg-gradient-to-r from-pink-500 via-red-500 to-yellow-500 p-[2px] text-white focus:outline-none focus:ring-4 focus:ring-yellow-300 active:text-opacity-75 font-medium"
          >
            <span className="block rounded-full px-8 py-3 text-sm font-medium hover:text-black hover:bg-transparent">
              Submit
            </span>
          </button>
        </div>
      </form>
    </section>
  );
}
