import { useEffect, useState } from "react";
import useAToken from "./UIAuth.tsx";
import { Link } from "react-router-dom";

const FridgeList = ({ convertTimeFormat }) => {
  const [fridges, setFridges] = useState([]);
  const { token } = useAToken();
  const [q, setQ] = useState("");
  const [showInactive, setShowInactive] = useState(false);
  const [searchParam] = useState([
    "fridge_name",
    "location",
    "neighborhood.neighborhood_name",
  ]);
  const title = "List of Fridges";

  async function getFridgeList() {
    const url = `${process.env.REACT_APP_API_HOST}/fridges`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setFridges(data);
    } else {
      console.error(response);
    }
  }

  useEffect(() => {
    getFridgeList();
    document.title = title;
    // eslint-disable-next-line
  }, []);

  function search(fridges) {
    return fridges.filter((fridge) => {
      const isActive = showInactive ? true : fridge.active;
      return (
        isActive &&
        searchParam.some((param) => {
          let value;
          if (param.includes(".")) {
            const nestedProps = param.split(".");
            value =
              fridge[nestedProps[0]] && fridge[nestedProps[0]][nestedProps[1]];
          } else {
            value = fridge[param];
          }
          return (
            value &&
            value.toString().toLowerCase().indexOf(q.toLowerCase()) > -1
          );
        })
      );
    });
  }

  return (
    <div className="p-6">
      <div className="flex justify-between items-center p-6">
        <h1 className="mt-8 mb-8 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl">
          <span className="text-transparent bg-clip-text bg-gradient-to-r from-teal-500 via-purple-500 to-green-500">
            Community Fridges
          </span>
        </h1>
        {token && (
          <Link to="/fridges/new">
            <button className="inline-block shrink-0 rounded-md border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-transparent hover:text-blue-600 focus:outline-none focus:ring active:text-blue-500">
              Add a Fridge
            </button>
          </Link>
        )}
      </div>
      <div className="flex flex-col sm:flex-row justify-between items-center p-6 search-wrapper">
        <div className="mb-4 sm:mb-0">
          <div className="relative">
            <label htmlFor="search-form">
              <input
                type="search"
                name="search-form"
                id="search-form"
                className="w-full rounded-md border-gray-200 py-2.5 pe-10 shadow-sm sm:text-sm"
                placeholder="Search for..."
                value={q}
                onChange={(e) => setQ(e.target.value)}
              />
              <span className="absolute inset-y-0 end-0 grid w-10 place-content-center">
                <button
                  type="button"
                  className="text-gray-600 hover:text-gray-700"
                >
                  <span className="sr-only">Search</span>

                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="h-4 w-4"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                    />
                  </svg>
                </button>
              </span>
              <span className="sr-only">Search Fridges here</span>
            </label>
          </div>
        </div>
        {token && (
          <div>
            <label>
              <input
                type="checkbox"
                className="w-4 h-4 text-pink-600 bg-gray-100 border-gray-300 rounded focus:ring-red-500 dark:focus:ring-red-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                checked={showInactive}
                onChange={(e) => setShowInactive(e.target.checked)}
              />
              <span className="w-full py-3 ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">
                Also Show Inactive Fridges
              </span>
            </label>
          </div>
        )}
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-12 ">
        {search(fridges).length > 0 ? (
          search(fridges).map((fridge) => {
            return (
              <div
                className="ease-in-out delay-150 duration-300 h-auto max-w-full rounded-lg shadow-lg hover:shadow-xl ring-1 ring-gray-900/5 hover:ring-2 hover:ring-indigo-700 hover:shadow-indigo-200"
                key={fridge.id}
              >
                <Link
                  to={`/fridges/${fridge.id}`}
                  className="block rounded-lg p-4"
                >
                  <img
                    alt={fridge.fridge_name}
                    src={fridge.fridge_picture}
                    className="h-56 w-full rounded-md object-cover"
                  />
                  <div className="mt-2 text-center">
                    <dl>
                      <div>
                        <dt className="sr-only">Fridge Name</dt>
                        <dd className="p-2 font-medium">
                          {fridge.fridge_name}
                        </dd>
                      </div>

                      <div>
                        <dt className="sr-only">Neighborhood</dt>
                        <dd className="text-navy-700 rounded-lg bg-gray-100 px-3 py-2 text-xs font-bold uppercase transition duration-200 dark:bg-white/10 dark:text-white">
                          In {fridge.neighborhood.neighborhood_name}
                        </dd>
                      </div>

                      <div>
                        <dt className="sr-only">Location</dt>
                        <dd className="p-2 select-all">{fridge.location}</dd>
                      </div>

                      <div>
                        <dt className="sr-only">Hours</dt>
                        <dd>
                          Opens:{" "}
                          <time dateTime={`${fridge.hours_start}`}>
                            {convertTimeFormat(fridge.hours_start)}
                          </time>
                        </dd>
                        <dd>
                          Closes:{" "}
                          <time dateTime={`${fridge.hours_end}`}>
                            {convertTimeFormat(fridge.hours_end)}
                          </time>
                        </dd>
                      </div>
                    </dl>
                  </div>
                  <div className="mt-6 flex items-center justify-center gap-8 text-xs">
                    <div className="sm:inline-flex sm:shrink-0 sm:items-center sm:gap-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="h-5 w-5 text-indigo-700"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M9 12.75L11.25 15 15 9.75M21 12c0 1.268-.63 2.39-1.593 3.068a3.745 3.745 0 01-1.043 3.296 3.745 3.745 0 01-3.296 1.043A3.745 3.745 0 0112 21c-1.268 0-2.39-.63-3.068-1.593a3.746 3.746 0 01-3.296-1.043 3.745 3.745 0 01-1.043-3.296A3.745 3.745 0 013 12c0-1.268.63-2.39 1.593-3.068a3.745 3.745 0 011.043-3.296 3.746 3.746 0 013.296-1.043A3.746 3.746 0 0112 3c1.268 0 2.39.63 3.068 1.593a3.746 3.746 0 013.296 1.043 3.746 3.746 0 011.043 3.296A3.745 3.745 0 0121 12z"
                        />
                      </svg>

                      <div className="mt-1.5 sm:mt-0">
                        <p className="text-gray-500">Status</p>
                        <p className="font-medium">
                          {fridge.active ? "Active" : "Inactive"}
                        </p>
                      </div>
                    </div>

                    <div className="sm:inline-flex sm:shrink-0 sm:items-center sm:gap-2">
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="1.5"
                        stroke="currentColor"
                        className="h-5 w-5 text-indigo-700"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M6.75 3v2.25M17.25 3v2.25M3 18.75V7.5a2.25 2.25 0 012.25-2.25h13.5A2.25 2.25 0 0121 7.5v11.25m-18 0A2.25 2.25 0 005.25 21h13.5A2.25 2.25 0 0021 18.75m-18 0v-7.5A2.25 2.25 0 015.25 9h13.5A2.25 2.25 0 0121 11.25v7.5"
                        />
                      </svg>

                      <div className="mt-1.5 sm:mt-0">
                        <p className="text-gray-500">Date Created</p>
                        <p className="font-medium">
                          {new Date(fridge.date_created).toDateString()}{" "}
                        </p>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            );
          })
        ) : (
          <p>No fridges match your search.</p>
        )}
      </div>
    </div>
  );
};

export default FridgeList;
